﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.IO;
[XmlRoot("Params")]
public class GameParams {
    [XmlAttribute("GameTime")]
    public float gameTime = 600f;
    [XmlAttribute("LvlUpPerformance")]
    public float lvlUpPerformance = 0.7f;
    [XmlAttribute("LvlDownPerformance")]
    public float lvlDownPerformance = 0.3f;

    [XmlAttribute("fallingSpeedMax")]
    public float fallingSpeedMax = 1000f;
    [XmlAttribute("fallingSpeedMin")]
    public float fallingSpeedMin = 10f;
    [XmlAttribute("OnStartSpeed")]
    public float speed = 50f;
    [XmlAttribute("LvlUpSpeedDelta")]
    public float speedUpDelta = 0.05f;
    [XmlAttribute("LvlDownSpeedDelta")]
    public float speedDownDelta = -0.1f;


    [XmlAttribute("MaxDelay")]
    public float maxDelay = 3f;
    [XmlAttribute("MinDelay")]
    public float minDelay = 0.05f;
    [XmlAttribute("OnStartDelay")]
    public float delay = 0.5f;
    [XmlAttribute("LvlUpDelayDelta")]
    public float delayUpDelta = -0.05f;
    [XmlAttribute("LvlDownDelayDelta")]
    public float delayDownDelta = 0.1f;

    [XmlAttribute("minQuantity")]
    public int minQuantity = 1;
    [XmlAttribute("maxQuantity")]
    public int maxQuantity = 1;
    [XmlAttribute("OnStartQuantity")]
    public int quantity = 1;
    [XmlAttribute("LvlUpQuantityDelta")]
    public int quantityUpDelta = 0;
    [XmlAttribute("LvlDownQuantityDelta")]
    public int quantityDownDelta = 0;

    [XmlAttribute("minCheckTime")]
    public float minCheckTime = 15f;
    [XmlAttribute("maxCheckTime")]
    public float maxCheckTime = 60f;
    public float shapeSize = 1f;
    [XmlAttribute("OnStartCheckTime")]
    public float checkTime = 30f;
    [XmlAttribute("LvlUpCheckTime")]
    public float lvlUpCheckTime = 20;
    [XmlAttribute("LvlDownCheckTime")]
    public float lvlDownCheckTime = 30;

    [XmlAttribute("maxWidth")]
    public float maxWidth = 4f;
    [XmlAttribute("minWidth")]
    public float minWidth = 1.1f;
    [XmlAttribute("OnStartWidth")]
    public float width = 1.5f;
    [XmlAttribute("LvlUpWidthDelta")]
    public float widthUpDelta = 0.05f;
    [XmlAttribute("LvlDownWidthDelta")]
    public float widthDownDelta = -0.1f;
    
    public GameParams()
    { 
    }

    public void Save()
    {
        String path = Application.dataPath + "/../params.xml";
        //Debug.Log(path);
        XmlSerializer serializer = new XmlSerializer(typeof(GameParams));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public GameParams Load()
    {
        String path = Application.dataPath + "/../params.xml";
        XmlSerializer serializer = new XmlSerializer(typeof(GameParams));
        try
        {
            //Debug.Log(Application.persistentDataPath);
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as GameParams;
            }
        }
        catch (Exception e)
        {
            GameParams gp = new GameParams();
            gp.Save();
            return gp;
        }
    }

}
