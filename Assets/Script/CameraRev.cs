﻿using UnityEngine;
using System.Collections;

public class CameraRev : MonoBehaviour {

	// Use this for initialization
	

    Camera camera;

    void Start()
    {
        camera = GetComponent<Camera>();

        Screen.SetResolution(Display.displays[0].systemWidth, Display.displays[0].systemHeight, true);
    }

    void OnPreCull()
    {
        camera.ResetWorldToCameraMatrix();
        camera.ResetProjectionMatrix();
        camera.projectionMatrix = camera.projectionMatrix * Matrix4x4.Scale(new Vector3(-1, 1, 1));
    }

    void OnPreRender()
    {
        GL.SetRevertBackfacing(true);
    }

    void OnPostRender()
    {
        GL.SetRevertBackfacing(false);
    }

    // GetComponent<Camera>().projectionMatrix = GetComponent<Camera>().projectionMatrix * Matrix4x4.Scale(new Vector3(1, 1, 1));
}
