﻿#define DEBUGMODE_OFF
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

public class KinectOneSkin : MonoBehaviour
{
    public float handReturn = 0.1f;
    public float wristReturn = 0.5f;

    public BodySourceView pointman;
    public ShapesController spc;

    public GameObject[] lastpointman;

    public CountDown countDown;

    public GameObject root;
    public float deltaY = -0.5f;

    public float moveKoef = 5;
    public float minRotateAngle = 0.5f;

    public GameObject FootLeft;
    public GameObject AnkleLeft;
    public GameObject KneeLeft;
    public GameObject HipLeft;

    public GameObject FootRight;
    public GameObject AnkleRight;
    public GameObject KneeRight;
    public GameObject HipRight;

    public GameObject HandTipLeft;
    public GameObject ThumbLeft;
    public GameObject HandLeft;
    public GameObject WristLeft;
    public GameObject ElbowLeft;
    public GameObject ShoulderLeft;

    public GameObject HandTipRight;
    public GameObject ThumbRight;
    public GameObject HandRight;
    public GameObject WristRight;
    public GameObject ElbowRight;
    public GameObject ShoulderRight;

    public GameObject SpineBase;
    public GameObject SpineMid;
    public GameObject SpineShoulder;
    public GameObject Neck;


    public Dictionary<string, GameObject> myObjects;


    Vector3 v1;
    Vector3 v2;

    private float timer = -1;

    private Quaternion[] lastQ;

    public float lerpValue = 0.2f;
    public float footValue = 1;
    //[Range(0, 1)]
    public float handLerpPersent = 0.00001f;
    // [Range(0, 1)]
    public float wirstLerpPersent = 0.3f;
    // [Range(0, 1)]
    public float elbowLerpPersent = 0.5f;
    // [Range(0, 1)]
    public float leg_armLerpPersent = 0.5f;
    public bool rotateTowards = true;

    public int player = 0;

    public Material BoneMaterial;
    public GameObject BodySourceManager;

    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;

    private List<Kinect.JointType> moveJoints;

    Vector3 firstBasePos;

    bool createPointman = false;

    void CreatePointMan()
    {
        if (!createPointman)
        {
            if (pointman.bodyyyyy != null)
            {
                List<GameObject> buf = new List<GameObject>(); ;
                for (int i = 0; i < pointman.bodyyyyy.transform.childCount; i++)
                {
                    GameObject g = new GameObject();
                    g.transform.position = pointman.bodyyyyy.transform.GetChild(i).transform.position;
                    g.name = pointman.bodyyyyy.transform.GetChild(i).name;


                    buf.Add(g);
                }

                lastpointman = buf.ToArray();

                createPointman = true;
            }


        }
    }

    void WritePoints()
    {
        /*if (pointman.bodyyyyy == null)
        {
            return;
        }*/
        // for (int i = 0; i < pointman.bodyyyyy.transform.childCount; i++)

        foreach (var item in lastpointman)
        {
            //Debug.Log(item.name);
            if (item.name != "Head" && myObjects[item.name] != null)
            {
                //Debug.Log("writed");
                item.transform.position = myObjects[item.name].transform.position;
            }


            /* if (myItem.name == item.name)
             {
                 item.transform.position = myItem.transform.position;
             }*/
        }

    }

    private Dictionary<Kinect.JointType, GameObject> _BoneMap = new Dictionary<Kinect.JointType, GameObject>()
    {
        /*{ Kinect.JointType.FootLeft, null },
        { Kinect.JointType.AnkleLeft, null },
        { Kinect.JointType.KneeLeft, null },
        { Kinect.JointType.HipLeft, null },

        { Kinect.JointType.FootRight, null },
        { Kinect.JointType.AnkleRight, null },
        { Kinect.JointType.KneeRight, null },
        { Kinect.JointType.HipRight, null },

        { Kinect.JointType.HandTipLeft, null },
        { Kinect.JointType.ThumbLeft, null },
        { Kinect.JointType.HandLeft, null },
        { Kinect.JointType.WristLeft, null },
        { Kinect.JointType.ElbowLeft, null},
        { Kinect.JointType.ShoulderLeft, null },

        { Kinect.JointType.HandTipRight, null },
        { Kinect.JointType.ThumbRight, null },
        { Kinect.JointType.HandRight, null },
        { Kinect.JointType.WristRight, null },
        { Kinect.JointType.ElbowRight, null },
        { Kinect.JointType.ShoulderRight, null },

        { Kinect.JointType.SpineBase, null},
        { Kinect.JointType.SpineMid, null },
        { Kinect.JointType.SpineShoulder, null },
        { Kinect.JointType.Neck,null },*/
    };

    List<Kinect.JointType> jTypes
     = new List<Kinect.JointType>()
        {

         { Kinect.JointType.FootLeft },
        { Kinect.JointType.AnkleLeft},
        { Kinect.JointType.KneeLeft },
        { Kinect.JointType.HipLeft },

        { Kinect.JointType.FootRight },
        { Kinect.JointType.AnkleRight },
        { Kinect.JointType.KneeRight },
        { Kinect.JointType.HipRight },

        { Kinect.JointType.HandTipLeft },
        { Kinect.JointType.ThumbLeft },
        { Kinect.JointType.HandLeft },
        { Kinect.JointType.WristLeft },
        { Kinect.JointType.ElbowLeft},
        { Kinect.JointType.ShoulderLeft },

        { Kinect.JointType.HandTipRight },
        { Kinect.JointType.ThumbRight },
        { Kinect.JointType.HandRight },
        { Kinect.JointType.WristRight },
        { Kinect.JointType.ElbowRight },
        { Kinect.JointType.ShoulderRight },

        { Kinect.JointType.SpineBase},
        { Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineShoulder },
        { Kinect.JointType.Neck },
};


    private Dictionary<Kinect.JointType, Quaternion> lastRootRotations = new Dictionary<Kinect.JointType, Quaternion>();


    private Dictionary<Kinect.JointType, Kinect.JointType> _JoinMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };


    GameParams gameParams = new GameParams();


    private Dictionary<Kinect.JointType, Kinect.JointType> MyMap;

    void OnLevelWasLoaded(int level)
    {

        spc.gameParams = gameParams.Load();
    }

    public Dictionary<GameObject, float> TimerObj = new Dictionary<GameObject, float>();

    void Start()
    {
        elsbowRotateValue = elbowLerpPersent;
        wirstRotateValue = wirstLerpPersent;
        handRotateValue = handLerpPersent;

        lastRootRotations = new Dictionary<Windows.Kinect.JointType, Quaternion>
        {
             { Kinect.JointType.FootLeft, Quaternion.identity },
        { Kinect.JointType.AnkleLeft,Quaternion.identity },
        { Kinect.JointType.KneeLeft, Quaternion.identity },
        { Kinect.JointType.HipLeft, Quaternion.identity},

        { Kinect.JointType.FootRight, Quaternion.identity },
        { Kinect.JointType.AnkleRight,Quaternion.identity},
        { Kinect.JointType.KneeRight, Quaternion.identity },
        { Kinect.JointType.HipRight, Quaternion.identity},

        { Kinect.JointType.HandTipLeft, Quaternion.identity },
        { Kinect.JointType.ThumbLeft, Quaternion.identity },
        { Kinect.JointType.HandLeft, Quaternion.identity},
        { Kinect.JointType.WristLeft,Quaternion.identity },
        { Kinect.JointType.ElbowLeft, Quaternion.identity},
        { Kinect.JointType.ShoulderLeft,Quaternion.identity},

        { Kinect.JointType.HandTipRight,Quaternion.identity},
        { Kinect.JointType.ThumbRight, Quaternion.identity},
        { Kinect.JointType.HandRight, Quaternion.identity},
        { Kinect.JointType.WristRight, Quaternion.identity},
        { Kinect.JointType.ElbowRight, Quaternion.identity},
        { Kinect.JointType.ShoulderRight,Quaternion.identity},

        { Kinect.JointType.SpineBase, Quaternion.identity},
        { Kinect.JointType.SpineMid, Quaternion.identity},
        { Kinect.JointType.SpineShoulder, Quaternion.identity },
        { Kinect.JointType.Neck,Quaternion.identity },
        };


        Dictionary<string, GameObject> _l = new Dictionary<string, GameObject>();
        //myObjects
        _l.Add("FootLeft", FootLeft);
        _l.Add("AnkleLeft", AnkleLeft);
        _l.Add("KneeLeft", KneeLeft);
        _l.Add("HipLeft", HipLeft);

        _l.Add("FootRight", FootRight);
        _l.Add("AnkleRight", AnkleRight);
        _l.Add("KneeRight", KneeRight);
        _l.Add("HipRight", HipRight);

        //mirror
        _l.Add("HandTipLeft", HandTipRight);
        _l.Add("ThumbLeft", ThumbRight);
        _l.Add("HandLeft", HandRight);
        _l.Add("WristLeft", WristRight);
        _l.Add("ElbowLeft", ElbowRight);
        _l.Add("ShoulderLeft", ShoulderRight);

        _l.Add("HandTipRight", HandTipRight);
        _l.Add("ThumbRight", ThumbRight);
        _l.Add("HandRight", HandRight);
        _l.Add("WristRight", WristRight);
        _l.Add("ElbowRight", ElbowRight);
        _l.Add("ShoulderRight", ShoulderRight);

        _l.Add("SpineBase", SpineBase);
        _l.Add("SpineMid", SpineMid);
        _l.Add("SpineShoulder", SpineShoulder);
        _l.Add("Neck", Neck);

        myObjects = _l;

        firstBasePos = SpineBase.transform.position;


        lastQ = new Quaternion[jTypes.Count];

        _BoneMap = new Dictionary<Kinect.JointType, GameObject>()
    {
        { Kinect.JointType.FootLeft, FootLeft },
        { Kinect.JointType.AnkleLeft, AnkleLeft },
        { Kinect.JointType.KneeLeft, KneeLeft },
        { Kinect.JointType.HipLeft, HipLeft },

        { Kinect.JointType.FootRight, FootRight },
        { Kinect.JointType.AnkleRight, AnkleRight },
        { Kinect.JointType.KneeRight, KneeRight },
        { Kinect.JointType.HipRight, HipRight },

        { Kinect.JointType.HandTipLeft, HandTipLeft },
        { Kinect.JointType.ThumbLeft, ThumbLeft },
        { Kinect.JointType.HandLeft, HandLeft },
        { Kinect.JointType.WristLeft, WristLeft },
        { Kinect.JointType.ElbowLeft, ElbowLeft},
        { Kinect.JointType.ShoulderLeft, ShoulderLeft },

        { Kinect.JointType.HandTipRight, HandTipRight },
        { Kinect.JointType.ThumbRight, ThumbRight },
        { Kinect.JointType.HandRight, HandRight },
        { Kinect.JointType.WristRight, WristRight },
        { Kinect.JointType.ElbowRight, ElbowRight },
        { Kinect.JointType.ShoulderRight, ShoulderRight },

        { Kinect.JointType.SpineBase, SpineBase},
        { Kinect.JointType.SpineMid, SpineMid },
        { Kinect.JointType.SpineShoulder, SpineShoulder },
        { Kinect.JointType.Neck,Neck },
    };

        foreach (var item in _BoneMap)
        {
            if (_BoneMap[item.Key] != null)
            {
                TimerObj.Add(_BoneMap[item.Key] as GameObject, 0.1f);
            }

        }

    }

    private float elsbowRotateValue, wirstRotateValue, handRotateValue;

    IEnumerator DownLerp(float max, float real)
    {
        real = max / 100;
        yield return new WaitForSeconds(0.1f);
        real = max;
    }


    public void Rotate(Kinect.Body[] data)
    {
        if (pointman.bodyyyyy == null)
        {
            return;
        }
        foreach (var item in data)
        {
            if (item.IsTracked)
            {



                if (countDown != null && !countDown.gameObject.activeSelf)
                {
                    countDown.StartTimer(10);
                    countDown.gameObject.SetActive(true);
                }

                int i = 0;
                foreach (var _type in jTypes)
                {
                    GameObject bone = _BoneMap[jTypes[i]];





                    if (bone != null)
                    {

                        Quaternion _new = getQ(item.JointOrientations[_type].Orientation);


                        if (i >= 0 && i <= 3)
                        {
                            _new = _new * Quaternion.Euler(0, 90 + 180, 0);

                        }
                        if (i >= 4 && i <= 7)
                        {
                            _new = _new * Quaternion.Euler(0, -90 + 180, 0);

                        }
                        if (i >= 11 && i <= 12)
                        {

                        }

                        float lerpKoef = 1;


                        if (jTypes[i] == Kinect.JointType.HandLeft)
                        {
                            lerpKoef = handLerpPersent;


                        }
                        if (jTypes[i] == Kinect.JointType.HandRight)
                        {
                            lerpKoef = handLerpPersent;
                        }
                        if (jTypes[i] == Kinect.JointType.WristLeft)
                        {
                            lerpKoef = wirstLerpPersent;

                        }
                        if (jTypes[i] == Kinect.JointType.WristRight)
                        {
                            lerpKoef = wirstLerpPersent;
                        }
                        if (jTypes[i] == Kinect.JointType.ElbowLeft)
                        {
                            lerpKoef = elbowLerpPersent;
                        }
                        if (jTypes[i] == Kinect.JointType.ElbowRight)
                        {
                            lerpKoef = elbowLerpPersent;
                        }

                        //Debug.Log(jTypes[i] + "; " + i + "; " + bone.name);



                        //Debug.Log(i + " : " + _type + lerpKoef);
                        Quaternion lastRoot = bone.transform.rotation;
                        Quaternion newRoot;
                        if (rotateTowards)
                        {
                            newRoot = Quaternion.RotateTowards(bone.transform.rotation, _new, lerpValue * lerpKoef * Time.deltaTime * 50);
                        }
                        else
                        {
                            newRoot = _new;
                        }






                        /*if (targetJoint.Value.TrackingState == Kinect.TrackingState.Inferred)
                        {
                            TimerObj[bone] = 0.1f;
                            Debug.Log(targetJoint.Value.JointType + " ;" + jTypes[i] + " ;" + bone.name);
                            bone.transform.rotation = Quaternion.identity;
                            continue;
                        }
                        else
                        {
                            TimerObj[bone] -= Time.deltaTime;
                            if (TimerObj[bone]>0)
                            {
                                continue;
                            }
                        }*/


                        Quaternion _end = Quaternion.identity;

                        switch (jTypes[i])//ElbowLeft WristLeft  HandLeft
                        {
                            case Kinect.JointType.ElbowLeft:
                                {
                                    Quaternion last = bone.transform.rotation;
                                    Vector3 v1 = Vector3.zero, v2 = Vector3.zero;
                                    for (int j = 0; j < pointman.bodyyyyy.transform.childCount; j++)
                                    {

                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "ShoulderLeft")
                                        {
                                            v1 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "ElbowLeft")
                                        {
                                            v2 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        bone.transform.up = v2 - v1;

                                    }

                                    /* if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 90)
                                     {
                                         elsbowRotateValue = elbowLerpPersent;
                                     }
                                     else
                                     {
                                         if (Vector3.Angle(-Vector3.forward, bone.transform.up) < 30)
                                         {
                                             elsbowRotateValue = elbowLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90/2;
                                         }
                                         else
                                         {
                                             elsbowRotateValue = elbowLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90;

                                         }

                                     }*/
                                    elsbowRotateValue = elbowLerpPersent;
                                    /*if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 80)
                                    {
                                        elsbowRotateValue = elbowLerpPersent;
                                    }
                                    else
                                    {
                                        //elsbowRotateValue = elbowLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90;
                                        //elsbowRotateValue = (Mathf.Atan(Vector3.Angle(-Vector3.forward, bone.transform.up) - 40) + Mathf.PI / 2) / Mathf.PI * elbowLerpPersent;
                                    }*/

                                    Debug.Log(Vector3.Angle(-Vector3.forward, bone.transform.up) + ";"+ elsbowRotateValue) ;

                                    _end = Quaternion.Lerp(last, bone.transform.rotation, elsbowRotateValue * Time.deltaTime);
                                    _end = Quaternion.Lerp(_end, newRoot, 0.25f);

                                    /*if (lastRootRotations[jTypes[i]]!=Quaternion.identity)
                                    {
                                        if (Quaternion.Angle(lastRootRotations[jTypes[i]],_end)>5)
                                        {
                                            _end = last;
                                            Debug.Log(Quaternion.Angle(lastRootRotations[jTypes[i]], _end));
                                        }

                                    }
                                    else
                                    {
                                        lastRootRotations[jTypes[i]] = _end;
                                    }*/
                                    //Debug.Log(Quaternion.Angle(last, _end) + " =" + jTypes[i]);


                                    /*if (Quaternion.Angle(last, _end) > 12) get ange perframe
                                    {
                                        
                                        _end = last;
                                        
                                    }*/

                                    /*Kinect.Joint sourceJoint = item.Joints[jTypes[i]];
                                    Kinect.Joint? targetJoint = null;

                                    if (_JoinMap.ContainsKey(jTypes[i]))
                                    {
                                        targetJoint = item.Joints[_JoinMap[jTypes[i]]];
                                    }

                                    if (sourceJoint.TrackingState == Kinect.TrackingState.Inferred || ((targetJoint.HasValue) && targetJoint.Value.TrackingState == Kinect.TrackingState.Inferred))
                                    {
                                        StartCoroutine(DownLerp(elbowLerpPersent, elsbowRotateValue));
                                        _end = Quaternion.RotateTowards(last, bone.transform.rotation, elsbowRotateValue * Time.deltaTime);
                                    }*/





                                    //_end = _end * Quaternion.Euler(0, 90, 0);
                                }
                                break;
                            case Kinect.JointType.ElbowRight:
                                {
                                    Quaternion last = bone.transform.rotation;
                                    Vector3 v1 = Vector3.zero, v2 = Vector3.zero;
                                    for (int j = 0; j < pointman.bodyyyyy.transform.childCount; j++)
                                    {

                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "ShoulderRight")
                                        {
                                            v1 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "ElbowRight")
                                        {
                                            v2 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        bone.transform.up = v2 - v1;

                                    }


                                    elsbowRotateValue = elbowLerpPersent;
                                    /*if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 80)
                                    {
                                        elsbowRotateValue = elbowLerpPersent;
                                    }
                                    else
                                    {
                                        //elsbowRotateValue = elbowLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90;
                                        //elsbowRotateValue = (Mathf.Atan(Vector3.Angle(-Vector3.forward, bone.transform.up) - 40) + Mathf.PI / 2) / Mathf.PI * elbowLerpPersent;
                                    }*/

                                    Debug.Log(Vector3.Angle(-Vector3.forward, bone.transform.up) + ";" + elsbowRotateValue);

                                    _end = Quaternion.Lerp(last, bone.transform.rotation, elsbowRotateValue * Time.deltaTime);
                                    _end = Quaternion.Lerp(_end, newRoot, 0.25f);

                                   
                                }
                                break;

                            case Kinect.JointType.WristLeft:
                                {

                                    Vector3 newElbowPos = Vector3.zero, neWristPos = Vector3.zero;



                                    for (int j = 0; j < pointman.bodyyyyy.transform.childCount; j++)
                                    {

                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "ElbowLeft")
                                        {
                                            newElbowPos = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "WristLeft")
                                        {
                                            neWristPos = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                    }

                                    Quaternion last = bone.transform.rotation;
                                    bone.transform.up = neWristPos - newElbowPos;
                                    //bone.transform.rotation = Quaternion.Lerp(last, bone.transform.rotation, wirstLerpPersent * Time.deltaTime);

                                    wirstRotateValue = wirstLerpPersent;
                                    /*if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 90)
                                    {
                                        wirstRotateValue = wirstLerpPersent;
                                    }
                                    else
                                    {
                                        wirstRotateValue = wirstLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90 / 2;

                                    }*/

                                    _end = Quaternion.Lerp(last, bone.transform.rotation, wirstRotateValue * Time.deltaTime);

                                    //Debug.Log(Vector3.Angle(_end * Vector3.up, _BoneMap[_JoinMap[jTypes[i]]].transform.up) + ";" + _BoneMap[_JoinMap[jTypes[i]]].name + ";" + bone.name + " \\" + bone.transform.up + "=" + bone.transform.rotation * Vector3.up);

                                    //Debug.Log(Quaternion.Angle(last, _end) + " =" + jTypes[i]);

                                   /* if (Vector3.Angle(_end * Vector3.up, _BoneMap[_JoinMap[jTypes[i]]].transform.up) > 90)
                                    {
                                        _end = last;

                                    }*/



                                    Kinect.Joint sourceJoint = item.Joints[jTypes[i]];
                                    Kinect.Joint? targetJoint = null;

                                    if (_JoinMap.ContainsKey(jTypes[i]))
                                    {
                                        targetJoint = item.Joints[_JoinMap[jTypes[i]]];
                                    }

                                    if (sourceJoint.TrackingState == Kinect.TrackingState.Inferred || ((targetJoint.HasValue) && targetJoint.Value.TrackingState == Kinect.TrackingState.Inferred))
                                    {
                                        StartCoroutine(DownLerp(wirstLerpPersent, wirstRotateValue));
                                        _end = Quaternion.RotateTowards(last, bone.transform.rotation, wirstRotateValue * Time.deltaTime);
                                    }

                                    //Quaternion _now = Quaternion.Lerp(last, bone.transform.rotation, 0.5f);
                                    //bone.transform.rotation = Quaternion.Lerp(bone.transform.rotation, last, wirstLerpPersent * Time.deltaTime);

                                }
                                break;
                            case Kinect.JointType.HandLeft:
                                {


                                    Vector3 v1 = Vector3.zero, v2 = Vector3.zero;



                                    for (int j = 0; j < pointman.bodyyyyy.transform.childCount; j++)
                                    {

                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "WristLeft")
                                        {
                                            v1 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "HandLeft")
                                        {
                                            v2 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                    }

                                    //Debug.Log(Time.deltaTime);
                                    Quaternion last = bone.transform.rotation;
                                    bone.transform.up = v2 - v1;


                                    //bone.transform.rotation = Quaternion.Lerp(bone.transform.rotation, last, 0.5f);
                                    //Quaternion before = Quaternion.Lerp(last, newRoot, 0.5f);
                                    //Quaternion _now = Quaternion.Lerp(bone.transform.rotation, newRoot, 1 * Time.deltaTime); // not rotate = 1
                                    //bone.transform.rotation = _now;

                                    handRotateValue = handLerpPersent;
                                    /*if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 90)
                                    {
                                        handRotateValue = handLerpPersent;
                                    }
                                    else
                                    {
                                        handRotateValue = handLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90 / 2;

                                    }*/

                                    _end = Quaternion.Lerp(last, bone.transform.rotation, handRotateValue * Time.deltaTime);
                                    //_end = Quaternion.Lerp(_end, newRoot, 0.1f);

                                    if (Vector3.Angle(_end * Vector3.up, _BoneMap[_JoinMap[jTypes[i]]].transform.up) > 80)
                                    {
                                        _end = last;
                                        //Debug.Log(Vector3.Angle(_end*Vector3.up, _BoneMap[_JoinMap[jTypes[i]]].transform.up) + ";" + _BoneMap[_JoinMap[jTypes[i]]].name + ";" + bone.name+ " \\" + bone.transform.up + "=" + bone.transform.rotation * Vector3.up);
                                    }


                                    /*Kinect.Joint sourceJoint = item.Joints[jTypes[i]];
                                    Kinect.Joint? targetJoint = null;

                                    if (_JoinMap.ContainsKey(jTypes[i]))
                                    {
                                        targetJoint = item.Joints[_JoinMap[jTypes[i]]];
                                    }

                                    if (sourceJoint.TrackingState == Kinect.TrackingState.Inferred || ((targetJoint.HasValue) && targetJoint.Value.TrackingState == Kinect.TrackingState.Inferred))
                                    {
                                        StartCoroutine(DownLerp(handLerpPersent, handRotateValue));
                                        _end = Quaternion.RotateTowards(last, bone.transform.rotation, handRotateValue * Time.deltaTime);
                                    }*/

                                }
                                break;

                            case Kinect.JointType.WristRight:
                                {

                                    Vector3 newElbowPos = Vector3.zero, neWristPos = Vector3.zero;



                                    for (int j = 0; j < pointman.bodyyyyy.transform.childCount; j++)
                                    {

                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "ElbowRight")
                                        {
                                            newElbowPos = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "WristRight")
                                        {
                                            neWristPos = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                    }

                                    Quaternion last = bone.transform.rotation;
                                    bone.transform.up = neWristPos - newElbowPos;
                                    //bone.transform.rotation = Quaternion.Lerp(last, bone.transform.rotation, wirstLerpPersent * Time.deltaTime);


                                    wirstRotateValue = wirstLerpPersent;
                                    /*if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 90)
                                    {
                                        wirstRotateValue = wirstLerpPersent;
                                    }
                                    else
                                    {
                                        wirstRotateValue = wirstLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90 / 2;

                                    }*/

                                    _end = Quaternion.Lerp(last, bone.transform.rotation, wirstRotateValue * Time.deltaTime);

                                    
                                }
                                break;

                            case Kinect.JointType.HandRight:
                                {


                                    Vector3 v1 = Vector3.zero, v2 = Vector3.zero;



                                    for (int j = 0; j < pointman.bodyyyyy.transform.childCount; j++)
                                    {

                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "WristRight")
                                        {
                                            v1 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                        if (pointman.bodyyyyy.transform.GetChild(j).name == "HandRight")
                                        {
                                            v2 = pointman.bodyyyyy.transform.GetChild(j).transform.position;

                                        }
                                    }

                                 
                                    Quaternion last = bone.transform.rotation;
                                    bone.transform.up = v2 - v1;


                                    handRotateValue = handLerpPersent;
                                    /*if (Vector3.Angle(-Vector3.forward, bone.transform.up) > 90)
                                    {
                                        handRotateValue = handLerpPersent;
                                    }
                                    else
                                    {
                                        handRotateValue = handLerpPersent * (Vector3.Angle(-Vector3.forward, bone.transform.up)) / 90 / 2;

                                    }*/

                                    _end = Quaternion.Lerp(last, bone.transform.rotation, handRotateValue * Time.deltaTime);

                                    if (Vector3.Angle(_end * Vector3.up, _BoneMap[_JoinMap[jTypes[i]]].transform.up) > 80)
                                    {
                                        _end = last;
                                        //Debug.Log(Vector3.Angle(_end*Vector3.up, _BoneMap[_JoinMap[jTypes[i]]].transform.up) + ";" + _BoneMap[_JoinMap[jTypes[i]]].name + ";" + bone.name+ " \\" + bone.transform.up + "=" + bone.transform.rotation * Vector3.up);
                                    }
                                }
                                break;
                            case Kinect.JointType.SpineShoulder:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.RotateTowards(bone.transform.rotation, newRoot, Mathf.Clamp(lerpValue * Time.deltaTime * 50, 0.1f, 0.9f));
                                    }
                                }
                                break;
                            case Kinect.JointType.SpineBase:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.RotateTowards(bone.transform.rotation, newRoot, Mathf.Clamp(lerpValue * Time.deltaTime * 50, 0.1f, 0.9f));
                                    }
                                }
                                break;
                            case Kinect.JointType.SpineMid:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.RotateTowards(bone.transform.rotation, newRoot, Mathf.Clamp(lerpValue * Time.deltaTime * 50, 0.1f, 0.9f));
                                    }
                                }
                                break;
                            case Kinect.JointType.Neck:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.RotateTowards(bone.transform.rotation, newRoot, Mathf.Clamp(lerpValue * Time.deltaTime * 50, 0.1f, 0.9f));
                                    }
                                }
                                break;
                            case Kinect.JointType.FootRight:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.Lerp(bone.transform.rotation, newRoot,footValue * Time.deltaTime);
                                    }
                                }
                                break;
                            case Kinect.JointType.FootLeft:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.Lerp(bone.transform.rotation, newRoot,footValue * Time.deltaTime );
                                    }
                                }
                                break;
                            case Kinect.JointType.AnkleRight:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.Lerp(bone.transform.rotation, newRoot, footValue * Time.deltaTime );
                                    }
                                }
                                break;
                            case Kinect.JointType.AnkleLeft:
                                {
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.Lerp(bone.transform.rotation, newRoot, footValue * Time.deltaTime );
                                    }
                                }
                                break;

                            default:
                                {

                                    //bone.transform.rotation = Quaternion.Lerp(bone.transform.rotation, newRoot, lerpKoef * Time.deltaTime * 50);

                                    //Debug.Log(lerpValue * Time.deltaTime );
                                    if (newRoot != Quaternion.identity && bone.transform.rotation != Quaternion.identity)
                                    {
                                        _end = Quaternion.Lerp(bone.transform.rotation, newRoot, Mathf.Clamp(footValue * Time.deltaTime * 50, 0.1f, 0.9f));
                                    }

                                }
                                break;
                        }
                        //if (_end != Quaternion.identity)
                        {
                            bone.transform.rotation = _end;
                        }











                    }
                    i++;
                }


            }
        }
    }
    void Update()
    {
        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        timer -= Time.deltaTime;
        if (timer < 0)
        {
            Kinect.Body[] data = _BodyManager.GetData();
            if (data == null)
            {
                //root.transform.position = Vector3.up * -22;
                root.SetActive(false);
                GameObject.Find("spaceman.001").GetComponent<SkinnedMeshRenderer>().enabled = false;
                GameObject.Find("spaceman").GetComponent<SkinnedMeshRenderer>().enabled = false;
                return;
            }

            CreatePointMan();

            Rotate(data);

            // move base join
            bool _end = true;
            foreach (var item in data)
            {
                if (item.IsTracked)
                {
                    Vector3 v3 = getV(item.Joints[Kinect.JointType.SpineBase].Position);
                    root.transform.position = new Vector3(v3.x, 0, 0) * moveKoef + firstBasePos - Vector3.up * deltaY;
#if UNITY_EDITOR || DEBUGMODE
                    //Debug.Log(v3);
#endif

                    _end = false;
                }
            }
            if (_end)
            {
                root.SetActive(false);
                GameObject.Find("spaceman.001").GetComponent<SkinnedMeshRenderer>().enabled = false;
                GameObject.Find("spaceman").GetComponent<SkinnedMeshRenderer>().enabled = false;
                //root.transform.position = Vector3.up * -22;
            }
            else
            {
                root.SetActive(true);
                GameObject.Find("spaceman.001").GetComponent<SkinnedMeshRenderer>().enabled = true;
                GameObject.Find("spaceman").GetComponent<SkinnedMeshRenderer>().enabled = true;
                timer = 0.03f;
            }

            WritePoints();


        }


    }

    public Quaternion getQ(Kinect.Vector4 v4)
    {
        return new Quaternion(v4.X, v4.Y, v4.Z, v4.W);
    }

    public Vector3 getV(Kinect.CameraSpacePoint point)
    {
        return new Vector3(point.X, point.Y, point.Z);
    }

}
