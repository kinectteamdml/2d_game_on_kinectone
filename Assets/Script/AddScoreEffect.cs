﻿using UnityEngine;
using System.Collections;

public class AddScoreEffect : MonoBehaviour {
    public float lifeTime = 1f;
    SpriteRenderer sr;
	void Start () {
        sr = this.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position += Vector3.up * Time.deltaTime;
        lifeTime -= Time.deltaTime;
        Color c = sr.color;
        c.a = lifeTime;
        sr.color = c;
        if (lifeTime < 0f)
            Destroy(this.gameObject);
	}
}
